import java.util.Base64;
import java.util.UUID;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class base64 {

    void encode (String name)
    {
        try {
          
           System.out.println("Orignal String: "+name);
            // Encode using basic encoder
           String base64encodedString = Base64.getEncoder().encodeToString(
              name.getBytes("utf-8"));
           System.out.println("Base64 Encoded String :" + base64encodedString);
          
  
        } 
        catch(UnsupportedEncodingException e) {
           System.out.println("Error :" + e.getMessage());
        }
    }

    void decode( String name)
    {
        try {          
            System.out.println("Encoded String: "+name);
          
           // Decode
           byte[] base64decodedBytes = Base64.getDecoder().decode(name);
          
           System.out.println("Base64 Decoded String: " + new String(base64decodedBytes, "utf-8"));
          
  
        } catch(UnsupportedEncodingException e) {
           System.out.println("Error :" + e.getMessage());
        }
    }
   
   
    public static void main(String args[]) {

        base64 o = new base64();
        Scanner sc = new Scanner(System.in);
        System.out.print("1.Encode a string \n2.Decode a string \n\nEnter your choice:");
        int choice = sc.nextInt();
        System.out.println();
        if(choice == 1)
        {
            System.out.print("Enter the string to be encoded:");
            String name = sc.next();
            System.out.println();
            o.encode(name);
        }
        if(choice == 2)
        {
            System.out.print("Enter the string to be decoded:");
            String name = sc.next();
            System.out.println();
            o.decode(name);
        }
        sc.close();
   }
}